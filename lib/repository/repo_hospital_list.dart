import 'package:app_api_test1/config/config_api.dart';
import 'package:dio/dio.dart';
import '../model/hospital_list.dart';

class RepoHospitalList {
  final String _baseUrl = 'https://api.odcloud.kr/api/apnmOrg/v1/list?page={page}&perPage={perPage}&serviceKey=ffzbt251LaJ%2F7rHLJrCbVET2d8hg7FDVqSHWJ1HtP9TwtJF%2Fyr20ixpa73bbbQwgZYp8br1jyNhYF9aUc6rEGQ%3D%3D';

  //미래에 HospitalList가 올거야라는 말이다 //async = 비동기  list가 언제 올지 모르는데 일일이 확인할 수 없으니까 비동기로 두어 알아서 가져오게해야됩니다. //{}안에 있는 값들은 list를 가지고 올 때 기본값으로 설정해주는 것입니다. 입력이 된게 없다면 저렇게 가지고 온다는 것입니다.
  Future<HospitalList> getList({int page = 1, int perPage = 10, String searchArea = '전체'}) async {
    String _resultUrl = _baseUrl.replaceAll('{page}', page.toString());
    _resultUrl = _resultUrl.replaceAll('{perPage}', perPage.toString());
    if(searchArea != '전체') {
      _resultUrl = _resultUrl + '&cond%5BorgZipaddr%3A%3ALIKE%5D=${Uri.encodeFull(searchArea)}';
    }





      Dio dio = Dio();

    final response = await dio.get(
      _resultUrl,
      options: Options(
        followRedirects: false, //내가 설정한 페이지가 아닌이상 다른 페이지로 따라가지말라는듯
        validateStatus: (status) {
          if(status == 200) { //정상
            return true;
          }else { //비정상
            return false;
          }
        }
      )
    );

    return HospitalList.fromJson(response.data);
  }
}
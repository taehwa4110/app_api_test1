import 'package:app_api_test1/model/hospital_list_item.dart';

class HospitalList {
  int page;
  int perPage;
  int totalCount;
  int currentCount;
  int matchCount;
  List<HospitalListItem>? data; //? = null 허용

  HospitalList(
      this.page,
      this.perPage,
      this.totalCount,
      this.currentCount,
      this.matchCount,
  {this. data}); // null이 올수도 있는 데이터는 {안에 넣어주어야합니다}

  factory HospitalList.fromJson(Map<String, dynamic> json) {
    return HospitalList(
      json['page'],
      json['perPage'],
      json['totalCount'],
      json['currentCount'],
      json['matchCount'],
      data: json['data'] != null ? (json['data'] as List).map((e) => HospitalListItem.fromJson(e)).toList() : null //list를 하나씩 펼치면서 변환을하고 다시 to list
    );
  }

}
import 'package:flutter/material.dart';

class ComponentNoContents extends StatelessWidget {
  const ComponentNoContents({super.key, required this.icon, required this.msg}) ;

  final IconData icon; //Icon의 자료형은 IconData이다
  final String msg;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(
              icon,
              size: 60, //size 값 뒤에 dp 생략
          ),
          const SizedBox(
            height: 15,
          ),
          Text(
              msg,
              style: const TextStyle(
                fontSize: 14
              ),
          ),
        ],
      ),
    );
  }
}

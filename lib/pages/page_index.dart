import 'package:app_api_test1/components/component_count_title.dart';
import 'package:app_api_test1/components/component_list_item.dart';
import 'package:app_api_test1/components/component_no_contents.dart';
import 'package:app_api_test1/repository/repo_hospital_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import '../model/hospital_list_item.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  final _scrollController = ScrollController(); //무한 스크롤을 하기 위해서는 스크롤 동작에 관여해야되기 때문에  ScrollController가 필요합니다

  final _formKey = GlobalKey<FormBuilderState>();
  bool _areaHasError = false;
  var areaOptions = ['전체', '서울특별시', '경기도 안산시', '인천광역시'];

  List<HospitalListItem> _list = [];
  int _page = 1; //현재 페이지
  int _totalPage = 1; //총 페이지 개수
  int _perPage = 10; //한 페이지 당 보여줄 아이템 개수
  int _totalCount = 0; //총 아이템 개수
  int _currentCount = 0; //현재 보여지고 있는 아이템 번호
  int _matchCount = 0; //검색 결과 개수
  String _searchArea = '전체';


  @override
  void initState() { //상태를 초기화 시킨다
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent) { //스크롤의 위치가 맨 끝에 다으면 _loadItems를 한번더 실행시켜주세요 라는 것이다
        _loadItems();
      }

    });
    _loadItems(); //들어온 값으로 기본값으로 설정한다.
  }

  Future<void> _loadItems({bool reFresh = false}) async { //함수가 나중에 값이 들어온다
    if (reFresh) {
      _list = [];
      _page = 1;
      _totalPage = 1;
      _perPage = 10;
      _totalCount = 0;
      _currentCount = 0;
      _matchCount = 0;
    }

    if (_page <= _totalPage) { //지금 페이지가 총 페이지보다 같거나 작으면
      await RepoHospitalList() //들어올 때까지 기다리다 RepoHospitalList에 리스트를 가져온다
          .getList(page: _page, searchArea: _searchArea)
          .then((res) => {  //result 약자 res // then 은 성공하면  가져온것들을 res라고 부를거고요
        setState((){ // 성공했으면 실행
          _totalPage = (res.matchCount / res.perPage).ceil(); //ceil은 올림
          _perPage = res.perPage;
          _totalCount = res.totalCount;
          _currentCount = res.currentCount;
          _matchCount = res.matchCount;

          _list = [..._list, ...?res.data]; //[]는 배열 ...list는 기본으로 들어갈 값이고 ?...data는 추가할 값입니다. 그니까 계속 추가되는것입니다

          _page++;
        })
      }).catchError((err) => { // 실패하면 실행
        debugPrint(err)
      });
    }
    
    if(reFresh) {
     _scrollController.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOut); //1000밀리 세컨드가 1초니까 300밀리세컨드는 0.3초입니다  //curve는 모션을 말한다
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('테스트'),
      ),
      body: ListView( //바닥에 다았는지 안다았는지 감시를 시켜야되는데 누구한테 권한을 주냐면? //ListView는 스스로 스크롤을 할 수 있으므로 SingleChildScrollView는 필요가 없습니다
        controller: _scrollController, //ListView에 컨트롤러가 있으니까 애한테 controller를 넣어줍니다
        children: [
          _buildBody(),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: CircularNotchedRectangle(),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,  //자동체크
          child: FormBuilderDropdown<String>(
            // autovalidate: true,
            name: 'area',
            decoration: const InputDecoration(
              labelText: '지역',
              hintText: '지역 선택',
            ),
            items: areaOptions
                  .map((area) => DropdownMenuItem(
                        alignment: AlignmentDirectional.center,
                        value: area,
                        child: Text(area),
                      ))
                  .toList(),
              onChanged: (val) {
              setState(() {
                _searchArea = _formKey.currentState!.fields['area']!.value!;
                _loadItems(reFresh: true);
              });
            },
            valueTransformer: (val) => val?.toString(),
          ),
        )
      ),
    );
  }

  Widget _buildBody() {
    if (_matchCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(icon: Icons.ac_unit, count: _matchCount, unitName: "개", itemName: '병원'),
          ListView.builder(
            physics: const NeverScrollableScrollPhysics(), //여기서 scroll을 사용하지 않겟다 위에 ListView에서 하겠다
            shrinkWrap: true,
            itemCount: _list.length, //현재 가진 리스트의 개수만큼 반복해야 되는거니까 matchCount가 아니라 list
            itemBuilder: (_, index) => ComponentListItem(
                item: _list[index],
                callback: () {}
            ),

          )

        ],
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 50, //-50한 이유는 그대로 가져가면 appbar의 높이도 포함되는 것이기 때문에 보통 appbar의 높이가 50이므로 -50을 한 것입니다
        child: const ComponentNoContents(icon: Icons.dangerous_rounded, msg: '데이터가 없습니다.'),
      );
    }
  }

}

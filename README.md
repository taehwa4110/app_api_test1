### 코로나 백신 예약 가능 병원 조회 APP
***
(개인프로젝트) 코로나 백신 예약이 가능한 병원을 조회해주는 APP
openAPI 사용
***

### Language
```
Flutter 3.3.2
Dart 2.18.1
```

### 기능
```
* 병원조회
```

### 앱 화면
>* 병원 조회 화면
>
>![app_hosptial_list](./images/app-hospital-list.png)